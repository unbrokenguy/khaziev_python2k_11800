import os
import re
argv = input().split(" ")
argv1 = argv[0]
argv2 = argv[1]
argv3 = argv[2]

def grep(dir, regexp, ext):
	listdir = os.listdir(dir)
	# print(listdir)
	for i in listdir:
		if os.path.isdir(dir + "\\" + i):
			grep(dir + "\\" + i, regexp, ext)
		else:
			file = open(dir + "\\" + i)
			fileName = i
			ext_ = fileName[fileName.find(".")+1:]
			if ext_ == ext:
				pattern = re.compile(regexp)
				file = file.read()
				file = file.split("\n")
				count = 0
				for temp in file:
					count += 1
					if len(pattern.findall(temp)) > 0:
						print(dir + "\\" + fileName + " pattern found on line " + str(count) + ": " + temp)

grep(argv1, argv2, argv3)