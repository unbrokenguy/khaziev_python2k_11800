def isInt(f):
	def wrapper(num):
		if isinstance(num, int):
			return f(num)
		else:
			raise TypeError("Not int")
	return wrapper

def isPos(f):
	def wrapper(num):
		if num >= 0:
			return f(num)
		else:
			raise ValueError("Not pos")
	return wrapper

@isPos
@isInt
def fact(num):
	ans = 1
	for i in range(1, num + 1):
		ans *= i
	return	ans

print(fact(4))