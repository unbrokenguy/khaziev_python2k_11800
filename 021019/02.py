import random


class Random_Letter:
	def __init__(self):
		self.count = 0

	def __next__(self):
		return chr(ord('a') + (int(random.random()*100)) % 26)


r = Random_Letter()
for i in range(10):
	print(next(r))