def isInt(f):
    def wrapper(num):
        if isinstance(num, int):
            return f(num)
        else:
            raise TypeError("Not int")

    return wrapper


def isPos(f):
    def wrapper(num):
        if num >= 0:
            return f(num)
        else:
            raise ValueError("Not pos")

    return wrapper


def toRomanNum(f):
    def wrapper(num):
        x = f(num)
        arab_numbers = [1, 4, 5, 9, 10, 40, 50, 90, 100, 400, 500, 900, 1000]
        roman_numbers = ["I", "IV", "V", "IX", "X", "XL", "L", "XC", "C", "CD", "D", "CM", "M"]
        result = ""
        index = 12
        while x != 0:
            while x >= arab_numbers[index]:
                x -= arab_numbers[index]
                result += roman_numbers[index]
            index -= 1
        return result

    return wrapper


@toRomanNum
@isPos
@isInt
def fact(num):
    ans = 1
    for i in range(1, num + 1):
        ans *= i
    return ans


print(fact(5))
