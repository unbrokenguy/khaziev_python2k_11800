import pygame
from task1 import WHITE
from math import cos
from math import sin
from math import radians as rd

pygame.init()
sc = w, h = (600, 400)
screen = pygame.display.set_mode(sc)
clock = pygame.time.Clock()
angle = 12
current_angle = 0
a, b, c, d = [(-w // 12, h // 4),
              (w // 12, h // 4),
              (-w // 12, -h // 4),
              (w // 12, -h // 4),
              ]


def draw_clock(crd):
    pygame.draw.line(screen, (255, 0, 0),
                     (crd[0][0] + w // 2, h // 2 - crd[0][1]),
                     (crd[1][0] + w // 2, h // 2 - crd[1][1]), 3)
    pygame.draw.line(screen, (0, 255, 0),
                     (crd[0][0] + w // 2, h // 2 - crd[0][1]),
                     (crd[3][0] + w // 2, h // 2 - crd[3][1]), 3)
    pygame.draw.line(screen, (0, 0, 255),
                     (crd[1][0] + w // 2, h // 2 - crd[1][1]),
                     (crd[2][0] + w // 2, h // 2 - crd[2][1]), 3)
    pygame.draw.line(screen, WHITE,
                     (crd[2][0] + w // 2, h // 2 - crd[2][1]),
                     (crd[3][0] + w // 2, h // 2 - crd[3][1]), 3)


def rotate():
    return [
        (a[0] * cos(rd(current_angle)) - a[1] * sin(rd(current_angle)),
         a[0] * sin(rd(current_angle)) + a[1] * cos(rd(current_angle))),
        (b[0] * cos(rd(current_angle)) - b[1] * sin(rd(current_angle)),
         b[0] * sin(rd(current_angle)) + b[1] * cos(rd(current_angle))),
        (c[0] * cos(rd(current_angle)) - c[1] * sin(rd(current_angle)),
         c[0] * sin(rd(current_angle)) + c[1] * cos(rd(current_angle))),
        (d[0] * cos(rd(current_angle)) - d[1] * sin(rd(current_angle)),
         d[0] * sin(rd(current_angle)) + d[1] * cos(rd(current_angle)))
    ]


while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            exit()
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_RIGHT:
                current_angle -= angle
            if event.key == pygame.K_LEFT:
                current_angle += angle
    screen.fill((0, 0, 0))
    draw_clock(rotate())
    pygame.display.update()
    clock.tick(60)
