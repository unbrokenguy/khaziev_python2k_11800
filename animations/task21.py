import pygame
from task1 import WHITE
from task1 import GREY

pygame.init()
screen = pygame.display.set_mode((1920, 600))
clock = pygame.time.Clock()

nums = 13
rects = [(80 * x + 5, 25, 75, 50) for x in range(nums)]
one_rect = 100 / nums
percent = 0


def move_order(x):
    global percent
    percent = (percent + x - 1) % 100 + 1
    print(percent)
    ans = 0
    for i in range(nums):
        if percent <= i * one_rect:
            break
        ans += 1
    return ans


how_many = 0
while True:
    pygame.time.delay(500)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            exit()
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_RIGHT:
                how_many = move_order(5)
            if event.key == pygame.K_LEFT:
                how_many = move_order(-5)

    screen.fill((0, 0, 0))
    color = GREY
    for i in range(1, nums + 1):
        if i > how_many:
            color = WHITE
        pygame.draw.rect(screen, color, rects[i - 1])
    pygame.display.update()
    clock.tick(60)
