import pygame

pygame.init()
screen = pygame.display.set_mode((1000, 400))
clock = pygame.time.Clock()

number_of_rect = 10
rects = [(80 * x + 5, 25, 75, 50) for x in range(number_of_rect)]
WHITE = (255, 255, 255)
GREY = (150, 150, 150)
order = [WHITE for x in range(number_of_rect)]
index_of_order = 0


def move_order():
    global index_of_order
    global order
    for index in range(number_of_rect):
        order[number_of_rect - index - 1] = order[number_of_rect - index - 2]
    if index_of_order < number_of_rect:
        order[0] = GREY
    else:
        order[0] = WHITE
    index_of_order = (index_of_order + 1) % (2 * number_of_rect)


while True:
    pygame.time.delay(500)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            exit()

    screen.fill((0, 0, 0))
    for i in range(number_of_rect):
        pygame.draw.rect(screen, order[i], rects[i])
    move_order()
    pygame.display.update()
    clock.tick(60)
