import pygame
from task1 import WHITE

pygame.init()
screen = pygame.display.set_mode((1920, 600))
clock = pygame.time.Clock()

back_rect = (20, 20, 510, 50)

percent = 0

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            exit()
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_RIGHT:
                percent += 5
                if percent >= 100:
                    percent = 100
            if event.key == pygame.K_LEFT:
                percent -= 5
                if percent < 0:
                    percent = 0

    screen.fill((0, 0, 0))
    pygame.draw.rect(screen, WHITE, back_rect)
    pygame.draw.rect(screen, ((255 - percent * 2) % 256, (percent * 2) % 255, 0),
                     (back_rect[0] + 5, back_rect[1] + 5,
                      (back_rect[2] - 5) // 100 * percent,
                      back_rect[3] - 10))
    pygame.display.update()
    clock.tick(60)
