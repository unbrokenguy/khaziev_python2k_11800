import pygame
from task1 import WHITE
from math import cos
from math import sin
from math import radians as rd

pygame.init()
sc = w, h = (600, 400)
screen = pygame.display.set_mode(sc)
clock = pygame.time.Clock()
draw = pygame.draw.arc
tail, t_grow = 90, False
head, h_grow = 100, True

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            exit()
    screen.fill((0, 0, 0))
    draw(screen, (255, 0, 0), (50, 50, 300, 300), rd(tail), rd(head), 3)
    if t_grow:
        tail = tail % 360 + 8
        head = head % 360 + 1
        if abs(tail - head) < 10:
            head = (tail + 15) % 360
            h_grow = True
            t_grow = False
    if h_grow:
        head = head % 360 + 8
        tail = tail % 360 + 1
        if abs(head - tail) < 10:
            h_grow = False
            t_grow = True
    print(head, tail)
    pygame.display.update()
    clock.tick(30)
