import os
import datetime as dt
from mutagen.mp3 import MP3


# '%b %d %Y %I:%M%p' Sep 29 2019 7:40AM
args = input().split(",")
path = args[0]
date1 = dt.datetime.strptime(args[1], "%b %d %Y")
date2 = dt.datetime.strptime(args[2], "%b %d %Y")
folder = []
for i in os.walk(path):
    folder.append(i)

paths = []
for address, dirs, files in folder:
    for file in files:
        if file.split(".")[1] == "mp3":
            creation_time = dt.datetime.fromtimestamp(os.stat(address + '/' + file).st_mtime)
            if date1 <= creation_time <= date2:
                paths.append((address, file))

playlist = open("playlist.m3u", 'w')
playlist.write("#EXTM3U\n")
for file in paths:
    print(file)
    playlist.write("#EXTINF:" + str(int(MP3(file[0] + '/' + file[1]).info.length)) + "," + file[1].split(".")[0] + "\n")
    playlist.write(file[0] + '\\' + file[1] + "\n")
