import sys
import random


def timetable(lessons, rooms, schedule):
    ttable = open("timetable.txt", 'w')
    week = open("week.txt", encoding='utf-8').read().split("\n")
    for day in week:
        number_of_classes = random.randint(1, 7)
        for i in range(number_of_classes):
            ttable.write(day + ", " + schedule[i] + ", " + random.choice(lessons) + ", " + random.choice(rooms) + "\n")


if __name__ == "__main__":
    if len(sys.argv) != 7:
        raise Exception("Incorrect number of parameters")
    if "--rooms" not in sys.argv or "--lessons" not in sys.argv or "--schedule" not in sys.argv:
        raise Exception("Wrong parameters")
    params = dict([(str(sys.argv[1]), str(sys.argv[2])),
                   (str(sys.argv[3]), str(sys.argv[4])),
                   (str(sys.argv[5]), str(sys.argv[6]))])

    lessons = open(params.get("--lessons"), encoding='utf-8').read().split("\n")
    rooms = open(params.get("--rooms"), encoding='utf-8').read().split("\n")
    schedule = open(params.get("--schedule"), encoding='utf-8').read().split("\n")
    timetable(lessons, rooms, schedule)
# день недели, время начала пары, название пары, номер кабинета
