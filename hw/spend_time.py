import datetime
import time

WEEK = {"MON": 0,
        "TUE": 0,
        "WED": 0,
        "THU": 0,
        "FRI": 0,
        "SAT": 0}

AMOUNT = 0

CLASSES = {}


def line_is_day(str):
    return WEEK.__contains__(str)


def class_duration(t):
    t = t.split("-")
    start = datetime.datetime.combine(datetime.datetime.today(),
                                      datetime.time(int(t[0].split(":")[0]),
                                                    int(t[0].split(":")[1])))
    end = datetime.datetime.combine(datetime.datetime.today(),
                                    datetime.time(int(t[1].split(":")[0]),
                                                  int(t[1].split(":")[1])))
    delta = str(end - start).split(":")
    return int(delta[0]) * 60 + int(delta[1])


timetable = open(input())
day = None
for line in timetable:
    line = line.replace("\n", "")
    print(line)
    if line_is_day(line):
        day = line
    else:
        duration, CLASS = line.split(" ")
        duration = class_duration(duration)
        AMOUNT += duration
        WEEK[day] = WEEK[day] + duration
        if CLASS in CLASSES.keys():
            CLASSES[CLASS] = CLASSES[CLASS] + duration
        else:
            CLASSES[CLASS] = duration

print("a)" + str(AMOUNT // 60) + " hours and " + str(AMOUNT - (AMOUNT // 60) * 60) + " minutes \n Write day:")
d = input()
print("b)" + str(WEEK[d] // 60) + " hours and " + str(WEEK[d] - (WEEK[d] // 60) * 60) + " minutes \n Write class:")
c = input()
print("c)" + str(CLASSES[c] // 60) + " hours and " + str(CLASSES[c] - (CLASSES[c] // 60) * 60) + " minutes")
