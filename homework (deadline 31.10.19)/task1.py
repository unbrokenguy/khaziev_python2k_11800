def not_string(f):
    def wrapper(x, y):
        try:
            x1, y2 = int(x), int(y)
            return f(x1, y2)
        except ValueError as error:
            raise error

    return wrapper


def positive(f):
    def wrapper(x, y):
        try:
            if x >= 0 or y >= 0:
                return f(x, y)
            else:
                raise ValueError("Negative numbers not allowed")
        except ValueError as error:
            raise error

    return wrapper


@not_string
@positive
def plus(x, y):
    return x + y


@not_string
@positive
def minus(x, y):
    return x - y


@not_string
@positive
def multiply(x, y):
    return x * y


@not_string
@positive
def divide(x, y):
    try:
        return x / y
    except ZeroDivisionError as error:
        raise error


def pn(s):
    try:
        temp = s.split(" ")
        if len(temp) != 3:
            raise Exception("Incorrect number of arguments")
        else:
            if temp[0] == "+":
                print(plus(temp[1], temp[2]))
            if temp[0] == "-":
                print(minus(temp[1], temp[2]))
            if temp[0] == "*":
                print(multiply(temp[1], temp[2]))
            if temp[0] == "/":
                print(divide(temp[1], temp[2]))
    except Exception as error:
        print(error)


for i in range(5):
    pn(input())
