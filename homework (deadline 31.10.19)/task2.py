import os


def cd(path):
    current_directory = os.path.abspath(os.curdir)
    try:
        if ":" in path:
            os.chdir(path)
            print(os.path.abspath(os.curdir))
        else:
            os.chdir(current_directory + "\\" + path)
            print(os.path.abspath(os.curdir))
    except FileNotFoundError as error:
        print(error)


def append(filename):
    print("write close() to save and close file")
    param = 'w'
    if os.path.exists(filename):
        param = 'a'
    file = open(filename, param)
    while True:
        string = input()
        if "close()" in string:
            string = string.replace("close()", "")
            file.write(string + "\n")
            file.close()
            break
        file.write(string + "\n")


print("write close() to quit")
while True:
    command = input().split(" ")
    if command[0] == "cd":
        cd(command[1])
    if command[0] == "append":
        append(command[1])
    if command[0] == "close()":
        break
